-- Set options
vim.opt.number = true -- Show line numbers
vim.opt.syntax = "on" -- Enable syntax highlighting
vim.opt.relativenumber = true -- show relative numbers
vim.opt.termguicolors = true

-- Set the colorscheme
vim.cmd("colorscheme quiet")
