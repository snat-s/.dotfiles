# .dotfiles

This are my dotfiles.

I have been using three pieces of software to develop
small things.

- tmux: Terminal multiplexer to have sane terminal management.
- vim: A barebones config.
- i3: Window manager to have sane window management.
